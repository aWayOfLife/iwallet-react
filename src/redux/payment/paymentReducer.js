import { ADD_PAYMENT_FAILURE, ADD_PAYMENT_REQUEST, ADD_PAYMENT_SUCCESS, FETCH_PAYMENTS_FAILURE, FETCH_PAYMENTS_REQUEST, FETCH_PAYMENTS_SUCCESS } from "./paymentTypes"

const initialState = {
    loading: false,
    payments:[],
    error:''
}

const reducer = (state=initialState,action) =>{
    switch(action.type){
        case FETCH_PAYMENTS_REQUEST:
            return{
                ...state,
                loading:true
            }
        
        case FETCH_PAYMENTS_SUCCESS:
            return{
                loading:false,
                payments:action.payload,
                error:''
            }
        
        case FETCH_PAYMENTS_FAILURE:
            return{
                loading:false,
                payments:[],
                error:action.payload
            }

            case ADD_PAYMENT_REQUEST:
                return{
                    ...state,
                    loading:true
                }
            
            case ADD_PAYMENT_SUCCESS:
                return{
                    ...state,
                    loading:false,
                    //payments:[...state.payments,action.payload],
                    error:''
                }
            
            case ADD_PAYMENT_FAILURE:
                return{
                    ...state,
                    loading:false,
                    error:action.payload
                }
        
        

        default:
            return state
    }
}

export default reducer