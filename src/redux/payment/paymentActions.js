import { ADD_PAYMENT_FAILURE, ADD_PAYMENT_REQUEST, ADD_PAYMENT_SUCCESS, FETCH_PAYMENTS_FAILURE, FETCH_PAYMENTS_REQUEST, FETCH_PAYMENTS_SUCCESS } from "./paymentTypes"
import axios from 'axios'
import db from '../../components/Firestore'
import { authError, authSuccess } from "../authentication/authenticationActions"


const devUrl = 'http://localhost:5000/api/'
const prodUrl = 'https://iwallet-react.herokuapp.com/api/'

export const fetchPaymentsRequest = () =>{
    return {type:FETCH_PAYMENTS_REQUEST}
}
export const fetchPaymentsSuccess = (payments) =>{
    return {
        type:FETCH_PAYMENTS_SUCCESS,
        payload:payments
        }
}
export const fetchPaymentsFailure = (error) =>{
    return {
        type:FETCH_PAYMENTS_FAILURE,
        payload:error
    }
}

export const fetchPayments = () =>{
    return (dispatch)=>{
        dispatch(fetchPaymentsRequest())

    
        axios.get(prodUrl+'payment')
            .then(sleeper(2500))
            .then(response => {
                const data =response.data
                dispatch(fetchPaymentsSuccess(data))
                
            })
            .catch(error =>{
                const errorMessage = error.message
                dispatch(fetchPaymentsFailure(errorMessage))
            })
    }
}

export const addPaymentRequest = () =>{
    return {type:ADD_PAYMENT_REQUEST}
}
export const addPaymentSuccess = (payment) =>{
    return {
        type:ADD_PAYMENT_SUCCESS,
        payload:payment
    }
}
export const addPaymentFailure = (error) =>{
    return {
        type:ADD_PAYMENT_FAILURE,
        payload:error
    }
}

export const addPayment = (payment, history) =>{
    const newPayment = payment
    return (dispatch, getState)=>{
        
        const config = tokenConfig(getState)
        console.log('config', config)
        if(!config) return dispatch(addPaymentFailure('PLEASE LOG IN TO CONTINUE'))

        dispatch(addPaymentRequest())


        axios.post(prodUrl+'payment', newPayment, config)
            .then(response => {
                dispatch(authSuccess(response.data))
                dispatch(addPaymentSuccess(newPayment))
                
                history.push("/")
            })
            .catch(error =>{
                const errorMessage = error.message

                dispatch(addPaymentFailure(errorMessage))
                if(error.response && error.response.status && error.response.status == 401) dispatch(authError('PLEASE LOGIN TO CONTINUE'))
                
            })
        }

        
}

function sleeper(ms) {
  return function(x) {
    return new Promise(resolve => setTimeout(() => resolve(x), ms));
  };
}

export const tokenConfig = getState =>{
    const accessToken = getState().authentication.accessToken
    const refreshToken = getState().authentication.refreshToken
        const config = {
            headers:{
                "Content-Type":"application/json",
            }
        }
        if(!accessToken || !refreshToken) return null
        config.headers['auth-token'] = accessToken
        config.headers['refresh-token'] = refreshToken
        
    return config
}

