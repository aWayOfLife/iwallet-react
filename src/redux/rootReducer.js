import {combineReducers} from 'redux'
import paymentReducer from './payment/paymentReducer'
import authenticationReducer from './authentication/authenticationReducer'

const rootReducer = combineReducers({
    payment:paymentReducer,
    authentication:authenticationReducer
})

export default rootReducer