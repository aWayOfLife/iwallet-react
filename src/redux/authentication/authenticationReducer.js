import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE,AUTH_SUCCESS, REGISTER_REQUEST, REGISTER_SUCCESS,REGISTER_FAILURE, USER_LOADED, USER_LOADING, AUTH_ERROR, LOGOUT_SUCCESS} from './authenticationTypes'

const initialState = {
    accessToken : localStorage.getItem('access-token'),
    refreshToken : localStorage.getItem('refresh-token'),
    isAuthenticated: false,
    isLoading: false,
    user: null,
    error:null
}


const reducer = (state=initialState,action) =>{
    switch(action.type){
        case USER_LOADING:
            return{
                ...state,
                isLoading:true,
                error:''
            }
        
        case USER_LOADED:
            return{
                ...state,
                isAuthenticated : true,
                isLoading : false,
                user: action.payload
            }
        case LOGIN_SUCCESS:
        case REGISTER_SUCCESS:
            localStorage.setItem('access-token', action.payload.accessToken)
            localStorage.setItem('refresh-token', action.payload.refreshToken)
            return{
                ...state,
                accessToken: action.payload.accessToken,
                refreshToken: action.payload.refreshToken,
                user: action.payload.user,
                isAuthenticated : true,
                isLoading : false,
                error:''
            }
        
        case AUTH_SUCCESS:
            localStorage.setItem('access-token', action.payload.accessToken)
            localStorage.setItem('refresh-token', action.payload.refreshToken)
            return{
                ...state,
                accessToken: action.payload.accessToken,
                refreshToken: action.payload.refreshToken,
                isAuthenticated : true,
                isLoading : false,
                error:''
            }
        
        case AUTH_ERROR:
        case LOGIN_FAILURE:
        case REGISTER_FAILURE:
        case LOGOUT_SUCCESS:
            localStorage.removeItem('access-token')
            localStorage.removeItem('refresh-token')
            return{
               ...state,
               accessToken:null,
               refreshToken:null,
               user:null,
               isAuthenticated:false,
               isLoading:false,
               error:action.payload? action.payload:null
            }
        default:
            return state
    }
}

export default reducer