import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE, AUTH_SUCCESS ,REGISTER_REQUEST, REGISTER_SUCCESS,REGISTER_FAILURE, USER_LOADED, USER_LOADING, AUTH_ERROR, LOGOUT_SUCCESS} from './authenticationTypes'
import axios from 'axios'

const devUrl = 'http://localhost:5000/api/'
const prodUrl = 'https://iwallet-react.herokuapp.com/api/'

export const userLoading = () =>{
    return {type:USER_LOADING}
}
export const userLoaded = (data) =>{
    return {
        type:USER_LOADED,
        payload:data
        }
}
export const authError = (error) =>{
    return {
        type:AUTH_ERROR,
        payload:error
    }
}

export const authSuccess = (data) =>{
    return {
        type:AUTH_SUCCESS,
        payload:data
    }
}

export const registerSuccess = (data) =>{
    return {
        type:REGISTER_SUCCESS,
        payload:data
    }
        
}

export const registerFailure = (error) =>{
    return {
        type:REGISTER_FAILURE,
        payload:error
    }
}

export const loginSuccess = (data) =>{
    return {
        type:LOGIN_SUCCESS,
        payload:data
    }
        
}

export const loginFailure = (error) =>{
    return {
        type:LOGIN_FAILURE,
        payload:error
    }
}

export const logoutSuccess = () =>{
    return {
        type:LOGOUT_SUCCESS
    }
        
}

//CHECK TOKEN AND LOAD USER

export const loadUser = (history)=>{
    return (dispatch, getState)=>{
        dispatch(userLoading())

        const config = tokenConfig(getState)
        if(!config) return dispatch(authError('PLEASE LOG IN TO CONTINUE'))

        axios.get(prodUrl+'user/',config)
            .then(response => {
                const data =response.data
                dispatch(userLoaded(data))
                //history.push("/")

            })
            .catch(error =>{
                const errorMessage = error.message
                dispatch(authError(errorMessage))
                //history.push("/login")
            })
    }
}

export const registerUser = (user, history)=>{
    return (dispatch)=>{

        const config = {
            headers:{
                "Content-Type":"application/json",
            }
        }

        axios.post(prodUrl+'user/register',user, config)
            .then(response => {
                const data = response.data
                dispatch(registerSuccess(data))
                history.push("/")

            })
            .catch(error =>{
                const errorMessage = error.message
                dispatch(registerFailure('REGISTRATION FAILED. PLEASE TRY AGAIN'))
                //history.push("/login")
            })
    }
}

export const loginUser = (user, history)=>{
    return (dispatch)=>{

        const data = {
            email:user.email,
            password:user.password
        }
        const config = {
            headers:{
                "Content-Type":"application/json",
            }
        }

        axios.post(prodUrl+'user/login',data, config)
            .then(response => {
                const data = response.data
                dispatch(loginSuccess(data))
                history.push("/")

            })
            .catch(error =>{
                const errorMessage = error.message
                dispatch(loginFailure("INCORRECT EMAIL OR PASSWORD"))
                //history.push("/login")
            })
    }
}

export const logoutUser = (history)=>{
    return (dispatch, getState)=>{

        const config = tokenConfig(getState)
        if(!config) {
            history.push("/login")
            return dispatch(logoutSuccess())
        }


        axios.delete(prodUrl+'user/logout',config)
            .then(response => {
                const data = response.data
                dispatch(logoutSuccess())
                history.push("/login")

            })
            .catch(error =>{
                const errorMessage = error.message
                dispatch(logoutSuccess())
                history.push(logoutSuccess)
            })
        
        history.push('/login')
    }
}




export const tokenConfig = getState =>{
    const accessToken = getState().authentication.accessToken
    const refreshToken = getState().authentication.refreshToken
        const config = {
            headers:{
                "Content-Type":"application/json",
            }
        }
        if(!accessToken || !refreshToken) return null
        config.headers['auth-token'] = accessToken
        config.headers['refresh-token'] = refreshToken
        
    return config
}