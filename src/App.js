import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import {BrowserRouter as Router, Route} from 'react-router-dom'
import Dashboard from './components/Dashboard';
import Nav from './components/Nav';
import {Provider} from 'react-redux'
import store from './redux/store'
import Transaction from './components/Transaction';
import PaymentDetails from './components/PaymentDetails';
import Login from './components/Login';
import Register from './components/Register';
import ProtectedRoute from './components/ProtectedRoute';
import PseudoProtectedRoute from './components/PseudoProtectedRoute';



function App() {
  return (
    <Router>
      <Provider store={store}>
        <div className="App">

          <Nav/>
          <PseudoProtectedRoute path="/login" exact component={Login}/>
          <PseudoProtectedRoute path="/register" exact component={Register}/>
          <Route path="/" exact component={Dashboard}/>
          <Route path="/payment/:id" component={PaymentDetails}/>
          <ProtectedRoute path="/transaction" component={Transaction}/>
        </div>
      </Provider>
    </Router>
  );
}



export default App
