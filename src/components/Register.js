import { Form, Formik } from "formik"
import FormikControl from './FormikControl'
import * as Yup from 'yup'
import {useHistory} from 'react-router-dom'
import { connect } from 'react-redux'
import {registerUser} from '../redux'

function Register(props) {

    const history = useHistory()
    const initialValues = {
        name:'',
        email:'',
        password:'',
    }

    const validationSchema = Yup.object({
        name:Yup.string().required('NAME IS REQUIRED').matches(/^[A-Za-z ]+$/, 'NAME MUST CONTAIN ALPHABETS'),
        email:Yup.string().required('EMAIL ID REQUIRED').email('EMAIL FORMAT INVALID'),
        password:Yup.string().required('PASSWORD REQUIRED').min(6,'PASSWORD LENGTH MUST BE AT LEAST 6'),
    })



    const onSubmit = values =>{
        props.registerUser(values, history)
    }
    const onClick = () =>{
        history.push('/login')
    }

    return (
        <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
            <Form className='container' autoComplete='off'>
    
                <div className='empty-state'>
                    <h4 className='mb-5'>REGISTER</h4>
                    <div className=' payment-input-panel col-md-6'>
                        <FormikControl control='input' type='text' label='NAME' name='name'/>
                        <FormikControl control='input' type='email' label='EMAIL' name='email'/>
                        <FormikControl control='input' type='text' label='PASSWORD' name='password'/>
                        <div className='button-container'>
                            <button className='cancel mt-4 mr-1' type='button' onClick={onClick}>LOGIN INSTEAD</button>
                            <button className='submit mt-4 ml-1' type='submit'>REGISTER</button>
                        </div>
                    </div>

                </div>

            </Form>
        </Formik>
    )
}


const mapStateToProps = state =>{
    return{
        isAuthenticated : state.authentication.isAuthenticated,
        error:state.authentication.error

    }
}

const mapDispatchToProps = dispatch =>{
    return{
        registerUser:(user,history) => dispatch(registerUser(user,history))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Register)
