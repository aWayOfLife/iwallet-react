import add from '../assets/add.svg'
import {useHistory} from 'react-router-dom'


function PaymentButton(props) {
    const history = useHistory()
    const onClick = () =>{
        history.push('/transaction')
    }
    return (
            <div onClick={onClick} className="add-payment-card">
                <div className="dark-band-button">
                    <h5>ADD PAYMENT</h5>      
                </div>
                <div className="fab">
                    <img src={add} alt=""></img>
                </div>
            </div>
    
    )
}


export default PaymentButton
