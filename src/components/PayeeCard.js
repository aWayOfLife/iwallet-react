function PayeeCard({payment}) {
    return (
        <div className="payee-card">
                <div className="payee-info">
                <p>PAYEE ACCOUNT NO</p> 
                <h5>{payment.payeeAccountNumber}</h5>    

                <p>PAYEE NAME</p> 
                <h6>{payment.payeeAccountName}</h6> 
                <p>TRANSACTION TYPE</p> 
                <h6>{payment.payeeTransactionType}</h6> 
                <div className="expiry-cvv">
                    <div>
                        <p>BANK NAME</p> 
                        <h6>{payment.payeeBankName}</h6>
                    </div>
                    <div className="cvv">
                        <p>IFSC CODE</p> 
                        <h6>{payment.payeeBankIFSC}</h6>
                    </div>
                </div> 
            </div>
        </div>

    )
}

export default PayeeCard
