import { Form, Formik } from "formik"
import FormikControl from './FormikControl'
import * as Yup from 'yup'
import {useHistory} from 'react-router-dom'
import { connect } from 'react-redux'
import {loginUser} from '../redux'

function Login(props) {

    const history = useHistory()
    const initialValues = {
        email:'',
        password:'',
    }

    const validationSchema = Yup.object({
        email:Yup.string().required('EMAIL ID REQUIRED').email('EMAIL FORMAT INVALID'),
        password:Yup.string().required('PASSWORD REQUIRED').min(6,'PASSWORD LENGTH MUST BE AT LEAST 6'),
    })



    const onSubmit = values =>{
        props.loginUser(values, history)
    }

    const onClick = () =>{
        history.push('/register')
    }

    return (
        <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
            <Form className='container' autoComplete='off'>
    
                <div className='empty-state'>
                    <h4 className='mb-5'>LOGIN</h4>
                    <div className=' payment-input-panel col-md-6'>
                        <FormikControl control='input' type='text' label='EMAIL' name='email'/>
                        <FormikControl control='input' type='text' label='PASSWORD' name='password'/>
                        <div className='button-container'>
                            <button className='cancel mt-4 mr-1' type='button' onClick={onClick}>REGISTER INSTEAD</button>
                            <button className='submit mt-4 ml-1' type='submit'>LOGIN</button>
                        </div>
                        <div className='empty-state mt-5'>
                            {props.isAuthenticated === false && props.error? <p>{props.error}</p>:null}
                        </div>
                    </div>
                </div>

            </Form>
        </Formik>
    )
}

const mapStateToProps = state =>{
    return{
        isAuthenticated : state.authentication.isAuthenticated,
        error:state.authentication.error
    }
}

const mapDispatchToProps = dispatch =>{
    return{
        loginUser:(user,history) => dispatch(loginUser(user,history))
 
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Login)