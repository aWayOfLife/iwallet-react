function FormikStep({children, ...props}) {

    
    return (
        <div className='row'>
            <div className='col-md-6'>
                    <div className='payment-input-panel'>
                        {children.slice(0,2)}
                    </div>
            </div>
            <div className='col-md-6'>
                    <div className='payment-input-panel'>
                        {children.slice(2,4)}
                    </div>
            </div>
        </div>
    )
}

export default FormikStep