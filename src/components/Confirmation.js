import PaymentCard from "./PaymentCard"
import { useHistory } from 'react-router-dom'

function Confirmation({payment, goToDashboard}) {
    const history = useHistory()

    return (
        <div className='empty-state'>
            <PaymentCard payment={payment}/>
            <h4>TRANSACTION IN PROCESS</h4>
            <h6>AMOUNT IS GETTING TRANSFERRED</h6> 
        </div>
    )
}

export default Confirmation
