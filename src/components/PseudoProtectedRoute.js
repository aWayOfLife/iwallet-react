import { Route, Redirect } from "react-router"
import { connect } from 'react-redux'

function PseudoProtectedRoute({ component: Component, ...rest }) {
    return (
              <Route {...rest} render={(props) => (
                !rest || rest.isAuthenticated===null? null:
                rest.isAuthenticated===false ? 
                <Component {...props} /> : <Redirect to={{ pathname: '/', state: { from: props.location }}} />)}>

            </Route>
   )
} 

const mapStateToProps = state =>{
    return{
        isAuthenticated : state.authentication.isAuthenticated
    }
}

export default connect(mapStateToProps,null)(PseudoProtectedRoute)