function Progress({step}) {
    return (
        <div className='progress-container'>
            <div className={step<0?'progress-unit':'progress-unit progress-complete'}>
                <div className='progress-box'>
                </div>
                <p className='progress-text'>PAYEE DETAILS</p>
            </div>
            <div className={step<1?'progress-unit':'progress-unit progress-complete'}>
                <div className='progress-box'>
                </div>
                <p className='progress-text'>PAYMENT DETAILS</p>
            </div>
            <div className={step<2?'progress-unit':'progress-unit progress-complete'}>
                <div className='progress-box'>
                </div>
                <p className='progress-text'>CONFIRMATION</p>
            </div>
        </div>
    )
}

export default Progress
