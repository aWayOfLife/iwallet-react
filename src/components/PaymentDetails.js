import PayeeCard from "./PayeeCard"
import PaymentCard from "./PaymentCard"
import {useEffect} from 'react'
import {connect} from 'react-redux'
import {fetchPayments} from '../redux'
import {useHistory} from 'react-router-dom'

function PaymentDetails({match:{params:{id}},paymentsData, fetchPayments}) {

    const payment = paymentsData.payments.find(payment => payment._id===id)

    useEffect(() => {
        if(!payment){
            fetchPayments()
        }
        
    }, [])
    

    return (
        <div className='container empty-state'>
            {payment?<div className='payment-details-container'>
                <PayeeCard payment={payment}/>
                <PaymentCard  payment={payment}/>
            </div>:!paymentsData.loading?(<div className="empty-state mt-5"><h4>No Records Found</h4></div>)
            :(<div className="empty-state mt-5"><h4>Loading..</h4></div>)}  
        </div>
    )
}

const mapStateToProps = state =>{
    return{
        paymentsData: state.payment
    }
}
const mapDispatchToProps = dispatch =>{
    return{
        fetchPayments:() => dispatch(fetchPayments())
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(PaymentDetails)

