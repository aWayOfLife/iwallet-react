function TextError (props) {
    return (
            <p className="error-text">{props.children}</p>
    )
        
  }
  
  export default TextError