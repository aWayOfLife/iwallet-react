import { Link } from 'react-router-dom'
import logo from '../assets/logo.svg'
import {useHistory} from 'react-router-dom'
import { connect } from 'react-redux'
import { loadUser, logoutUser } from '../redux'
import {useEffect} from 'react'


function Nav(props) {
    const history = useHistory()
    useEffect(() => {
        props.loadUser(history)
    }, [])
    const onLoginClick = () =>{
        history.push('/login')
    }
    const onLogoutClick = () =>{
        props.logoutUser(history)
    }
    return (
        <nav className="navbar sticky-top navbar-expand-lg navbar-light bg-white">
            <div className="container">
                <Link to='/' className="navbar-brand">
                    <img src={logo} width="100" height="60" alt="" loading="lazy"/>
                </Link>
                {props.isAuthenticated ? <button className='nav-button' type='button' onClick={onLogoutClick}>LOG OUT</button>:props.isAuthenticated!=null? <button className='nav-button' type='button' onClick={onLoginClick}>LOG IN</button>:null}
            </div>
        </nav>

    )
}

const mapDispatchToProps = dispatch =>{
    return{
        loadUser:(history) => dispatch(loadUser(history)),
        logoutUser:(history) => dispatch(logoutUser(history))
    }
}

const mapStateToProps = state =>{
    return{
        isAuthenticated : state.authentication.isAuthenticated
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Nav)

