import PaymentButton from "./PaymentButton"
import PaymentCard from "./PaymentCard"
import {useEffect} from 'react'
import {connect} from 'react-redux'
import {fetchPayments} from '../redux'
import Loader from "./Loader"

function Dashboard({paymentsData,fetchPayments}) {
    useEffect(() => {
        fetchPayments()
        
    }, [])
    return (
        
        <div className="container">

            
            {paymentsData.payments && paymentsData.payments.length>0?
            (<div className="filled-state">
                <div className="payment-card-container">
                    {paymentsData.payments.map((payment)=><PaymentCard key={payment._id} payment={payment}/>)}
                    <PaymentButton/>
                </div>
            </div>)
            :!paymentsData.loading?(<div className="empty-state">      
                <PaymentButton/>         
                <h4>YOU HAVE NOT MADE ANY PAYMENTS YET</h4>
                <h6>ONCE YOU MAKE SOME PAYMENTS THE DETAILS WILL APPEAR</h6>
            </div>):(<Loader/>)}
            
    
        </div>
    )
}

const mapStateToProps = state =>{
    return{
        paymentsData: state.payment
    }
}
const mapDispatchToProps = dispatch =>{
    return{
        fetchPayments:() => dispatch(fetchPayments())
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Dashboard)
