import {useHistory} from 'react-router-dom'
function PaymentCard({payment}) {
    const history = useHistory()
    const paymentCardClick = (id) =>{
        if(id){
            history.push(`/payment/${id}`)
        }
    }
    return (
        <div onClick={()=> paymentCardClick(payment._id)} className="payment-card">
            <div className="dark-band">
                <p>TRANSACTION AMOUNT</p> 
                <h5>{payment.payerAmount} USD</h5>    
            </div>
            <div className="card-info">
                <div>
                    <p>CARD NUMBER</p> 
                    <h6>{payment.payerCardNumber}</h6> 
                </div>
                <div>
                    <p>NAME</p> 
                    <h6>{payment.payerCardName}</h6> 
                </div>
                <div className="expiry-cvv">
                    <div>
                        <p>EXPIRY</p> 
                        <h6>{payment.payerCardExpiry}</h6>
                    </div>
                    <div className="cvv">
                        <p>CVV</p> 
                        <h6>{payment.payerCardCVV}</h6>
                    </div>
                </div> 
            </div>
        </div>

    )
}

export default PaymentCard
