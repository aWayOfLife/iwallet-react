import {Formik,Form, FormikConfig, FormikValues} from 'formik'
import {React, useState, useHistory} from 'react'
import Progress from './Progress'


function FormikStepper({children,goToDashboard, ...props}) {
    //const history = useHistory()
    const [step,setStep] = useState(0)
    const currentChild = children[step]
    const isLastStep = () =>{
        return step === children.length-2
    }
    const isLastStepDone = () =>{
        return step === children.length-1
    }

    
    return (
        <Formik  {...props} validationSchema={currentChild.props.validationSchema} onSubmit={async(values, helpers)=>{
            if(isLastStep()){
                await props.onSubmit(values, helpers)
                setStep(s=>s+1)
            }else{
                setStep(s=>s+1)
            }
            helpers.setTouched({})
        }}>
            <Form className='container' autoComplete='off'>
                <div>
                     <div className='row mb-5'>
                        <div className='col-md-6'>
                            <div className='payment-input-panel'>
                                <Progress step={step}/> 
                            </div>
                        </div>
                    </div>
                    <div>
                        {currentChild}
                    </div>

                    {!isLastStepDone()?(<div className='row mt-4'>
                        <div className="col-md-6 payment-cancel">
                            <div className='payment-input-panel'>
                                <button type="button" className="cancel" onClick={()=> isLastStep()? setStep(s=>s-1):goToDashboard()}>{isLastStep()? 'PREVIOUS STEP':'CANCEL PAYMENT'}</button>
                            </div>
                        </div>
                        <div className="col-md-6  payment-submit">
                            <div className='payment-input-panel'>
                                <button className="submit"   type="submit">{isLastStep()? 'CONFIRM PAYMENT':'GO TO PAYMENT'}</button>
                            </div>                             
                        </div>
                    </div>):null}
                </div>
            </Form>
        </Formik>
    )
}

export default FormikStepper
