function Loader() {
    return (
        <div className="loader-container">
            <svg className="loader" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 294 294">
                <g id="loader" transform="translate(-378.283 -64.189)">
                    <circle id="Ellipse_1" data-name="Ellipse 1" cx="147" cy="147" r="147" transform="translate(378.283 64.188)" fill="#2caffe"/>
                    <circle id="Ellipse_2" data-name="Ellipse 2" cx="73.5" cy="73.5" r="73.5" transform="translate(474.283 109.188)" fill="#353f81"/>
                </g>
            </svg>
            <p className="mt-3">LOADING</p>
        </div>
    )
}

export default Loader
