import { Route, Redirect } from "react-router"
import { connect } from 'react-redux'

function ProtectedRoute({ component: Component, ...rest }) {
    return (
              <Route {...rest} render={(props) => (
                rest.isAuthenticated === true ? 
                <Component {...props} /> : <Redirect to={{ pathname: '/login', state: { from: props.location }}} />)}>

            </Route>
   )
} 

const mapStateToProps = state =>{
    return{
        isAuthenticated : state.authentication.isAuthenticated
    }
}

export default connect(mapStateToProps,null)(ProtectedRoute)
