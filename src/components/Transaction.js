import * as Yup from 'yup'
import FormikControl from './FormikControl'
import FormikStepper from './FormikStepper'
import FormikStep from './FormikStep'
import Confirmation from './Confirmation'
import { connect } from 'react-redux'
import {addPayment} from '../redux'
import {useState} from 'react'
import {useHistory} from 'react-router-dom'


function Transaction(props) {
    const [payment,setPayment] = useState({})
    const history = useHistory()
    const initialValues = {
        payeeAccountName:'',
        payeeAccountNumber:'',
        payeeBankName:'',
        payeeBankIFSC:'',
        payeeTransactionType:'',
        payerCardNumber:'',
        payerCardName:'',
        payerCardExpiry:'',
        payerCardCVV:'',
        payerAmount:''
    }

    const validationSchemaStepOne = Yup.object({
        payeeAccountName:Yup.string().required('PAYEE ACCOUNT NAME REQUIRED').matches(/^[A-Za-z ]+$/, 'NAME MUST CONTAIN ALPHABETS'),
        payeeAccountNumber:Yup.string().required('PAYEE ACCOUNT NUMBER REQUIRED').matches(/^\d{11,16}$/,'ACCOUNT NUMBER MUST BE 11-16 DIGITS'),
        payeeBankName:Yup.string().required('PAYEE BANK NAME REQUIRED').matches(/^[A-Za-z ]+$/, 'BANK NAME MUST CONTAIN ALPHABETS'),
        payeeBankIFSC:Yup.string().required('PAYEE BANK IFSC REQUIRED').matches(/^[A-Za-z]{4}\d{7}$/,'IFSC MUST HAVE 4 ALPHABETS AND 7 DIGITS'),
        payeeTransactionType:Yup.string().required('TRANSACTION TYPE REQUIRED').equals(['NEFT','IMPS'],'INVALID TRANSACTION TYPE')
    })

    const validationSchemaStepTwo = Yup.object({
        payerCardNumber:Yup.string().required('CARD NUMBER REQUIRED').matches(/^\d{16}$/,'CARD NUMBER MUST BE 16 DIGITS'),
        payerCardName:Yup.string().required('CARD NAME REQUIRED').matches(/^[A-Za-z ]+$/, 'NAME MUST CONTAIN ALPHABETS'),
        payerCardExpiry:Yup.string().required('CARD EXPIRY REQUIRED').matches(/^(0[1-9]|1[0-2])\/\d{4}$/,'EXPIRY MUST BE IN MM/YYYY'),
        payerCardCVV:Yup.string().required('CARD CVV REQUIRED').matches(/^\d{3}$/,'CVV MUST BE 3 DIGITS'),
        payerAmount:Yup.string().required('AMOUNT REQUIRED').matches(/^[1-9][0-9]*$/, 'MUST BE A POSITIVE INTEGER')
    })


    const onSubmit = values =>{
        setPayment(values)
        props.addPayment(values, history)
    }

    const goToDashboard = () =>{
        history.push('/')
    }
    

    return (
        <FormikStepper goToDashboard={goToDashboard} initialValues={initialValues} onSubmit={onSubmit} enableReinitialize>
            <FormikStep  validationSchema={validationSchemaStepOne}>
                        <FormikControl control='input' type='text' label='PAYEE ACCOUNT NAME' name='payeeAccountName'/>
                        <FormikControl control='input' type='text' placeholder='11-16 DIGIT NUMBER' label='PAYEE ACCOUNT NUMBER' name='payeeAccountNumber'/>
                        <div className='payment-input-container'>
                            <FormikControl control='input' type='text' label='PAYEE BANK NAME' name='payeeBankName'/>
                            <FormikControl control='input' type='text' placeholder='ABCD1234567' label='PAYEE BANK IFSC' name='payeeBankIFSC'/>
                        </div>
                        <FormikControl control='input' type='text' placeholder='NEFT/IMPS' label='PAYEE TRANSACTION TYPE' name='payeeTransactionType'/> 
                      
            </FormikStep>
            <FormikStep validationSchema={validationSchemaStepTwo}>
                        <FormikControl control='input' type='text' placeholder='16 DIGIT NUMBER' label='CARD NUMBER' name='payerCardNumber'/>
                        <FormikControl control='input' type='text' label='NAME ON CARD' name='payerCardName'/>
                        <div className='payment-input-container'>
                            <FormikControl control='input' type='text' placeholder='MM/YYYY' label='CARD EXPIRY' name='payerCardExpiry'/>
                            <FormikControl control='input' type='text' label='CARD CVV' name='payerCardCVV'/>
                        </div>
                        <FormikControl control='input' type='text' label='TRANSACTION AMOUNT' name='payerAmount'/>
            </FormikStep>
            <Confirmation goToDashboard={goToDashboard} payment={payment} validationSchema={null}/>
        </FormikStepper>

        
    )
}


const mapDispatchToProps = dispatch =>{
    return{
        addPayment:(payment, history) => dispatch(addPayment(payment, history)),
    }
}


export default connect(null,mapDispatchToProps)(Transaction)
